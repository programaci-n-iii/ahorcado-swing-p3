package paneles;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ahorcado.JuegoSettings;

@SuppressWarnings("serial")
public class PanelMenu extends JPanel {
	private JPanel _panelAhorcado;
	private List<JPanel> _paneles;
	private JLabel _imagenFondo;

	public PanelMenu(JFrame frame) {
		setSize(640, 480);
		setLayout(null);

		_paneles = JuegoSettings.crearPaneles();

		JLabel tituloAhorcado = new JLabel("Ahorcado");
		tituloAhorcado.setFont(new Font("Dialog", Font.BOLD, 25));
		tituloAhorcado.setBounds(236, 12, 154, 35);
		add(tituloAhorcado);

		////////////////// Botones ///////////////////////////
		JButton botonJugar = JuegoSettings.OPCIONES_MENU.get(0);
		JButton botonSalir = JuegoSettings.OPCIONES_MENU.get(JuegoSettings.OPCIONES_MENU.size() - 1);

		for (int i = 0; i < JuegoSettings.OPCIONES_MENU.size(); i++) {
			JButton boton = JuegoSettings.OPCIONES_MENU.get(i);
			boton.setFont(new Font("Dialog", Font.BOLD, 15));
			boton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			boton.setBounds(10, 100 + i * 50, 150, 40);
			if (boton.equals(botonJugar)) {
				boton.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						_panelAhorcado = JuegoSettings.crearPanelSegunModalidad(
								((PanelIdioma) _paneles.get(1)).darIdiomaSeleccionado(),
								((PanelDificultad) _paneles.get(0)).darDificultadSeleccionada(),
								((PanelModalidad) _paneles.get(2)).darModalidadSeleccionada());

						removeAll();
						add(_panelAhorcado, BorderLayout.CENTER);
						revalidate();
						repaint();
					}

				});
				add(boton);
			} else if (boton.equals(botonSalir)) {
				boton.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						frame.dispose();
					}

				});
				add(boton);
			} else {
				boton.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent e) {
						mostrarPanel(darPanelQueSeEntraConBoton(boton));
					}

				});
				add(boton);
			}
		}

		add(_paneles.get(_paneles.size() - 1));

		//////// Fondo ///////////
		_imagenFondo = new JLabel(JuegoSettings.IMG_FONDO);
		_imagenFondo.setBounds(0, 0, 640, 480);
		add(_imagenFondo);
	}

	private void mostrarPanel(JPanel panelAgregar) {
		List<JPanel> panelesAquitar = darPanelesExcepto(panelAgregar);
		for (JPanel panel : panelesAquitar) {
			remove(panel);
		}
		remove(_imagenFondo);
		add(panelAgregar, BorderLayout.CENTER);
		add(_imagenFondo);
		revalidate();
		repaint();
	}

	private List<JPanel> darPanelesExcepto(JPanel panel) {
		List<JPanel> paneles = new ArrayList<>();
		for (JPanel p : _paneles) {
			if (!p.getClass().getName().equals(panel.getClass().getName()))
				paneles.add(p);
		}
		return paneles;
	}

	private JPanel darPanelQueSeEntraConBoton(JButton boton) {
		if (boton.equals(JuegoSettings.OPCIONES_MENU.get(0))) {
			return _panelAhorcado;
		}
		for (JPanel p : _paneles) {
			if (boton.equals(JuegoSettings.OPCIONES_MENU.get(_paneles.indexOf(p) + 1))) {
				return _paneles.get(_paneles.indexOf(p));
			}
		}
		return null;
	}
}

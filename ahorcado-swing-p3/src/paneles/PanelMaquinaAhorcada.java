package paneles;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

import ahorcado.JuegoSettings;
import ahorcado.MaquinaAhorcada;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Cursor;

@SuppressWarnings("serial")
public class PanelMaquinaAhorcada extends JPanel {
	private JTextField _inputPalabra;
	private JTextField _intentosEstimadosInput;
	private MaquinaAhorcada _maquina;
	private String _palabraInput;

	public PanelMaquinaAhorcada() {
		setSize(640, 480);
		setLayout(null);

		JLabel lblEscribaUnaPalabra = new JLabel("Escriba una palabra:");
		lblEscribaUnaPalabra.setBounds(12, 141, 185, 22);
		lblEscribaUnaPalabra.setFont(new Font("Dialog", Font.PLAIN, 20));
		add(lblEscribaUnaPalabra);

		_inputPalabra = new JTextField();
		_inputPalabra.setBounds(207, 139, 161, 30);
		_inputPalabra.setFont(new Font("Dialog", Font.PLAIN, 16));
		_inputPalabra.setColumns(10);
		add(_inputPalabra);

		JLabel lblEstimeCuntosIntentos = new JLabel("Estime cuántos intentos me llevará adivinar:");
		lblEstimeCuntosIntentos.setBounds(12, 205, 388, 21);
		lblEstimeCuntosIntentos.setFont(new Font("Dialog", Font.PLAIN, 20));
		add(lblEstimeCuntosIntentos);

		_intentosEstimadosInput = new JTextField();
		_intentosEstimadosInput.setBounds(403, 204, 30, 30);
		_intentosEstimadosInput.setColumns(10);
		add(_intentosEstimadosInput);

		JLabel msjGameOver = new JLabel("");
		msjGameOver.setBounds(12, 307, 584, 47);
		msjGameOver.setFont(new Font("Dialog", Font.BOLD, 20));
		add(msjGameOver);

		JButton btnArriesgar = new JButton("Arriesgar");
		btnArriesgar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnArriesgar.setFont(new Font("Dialog", Font.PLAIN, 20));
		btnArriesgar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				_palabraInput = _inputPalabra.getText();
				_maquina = new MaquinaAhorcada(_palabraInput, _intentosEstimadosInput.getText());
				_maquina.reproducirSonidoAdecuado();
				msjGameOver.setText(_maquina.mensajeGameOver());
			}

		});
		btnArriesgar.setBounds(443, 202, 117, 32);
		add(btnArriesgar);

		JLabel lblMquinaAhorcada = new JLabel("Máquina Ahorcada (o no)");
		lblMquinaAhorcada.setFont(new Font("Dialog", Font.BOLD, 25));
		lblMquinaAhorcada.setBounds(144, 28, 366, 30);
		add(lblMquinaAhorcada);

		JLabel lblTxtinformativo = new JLabel(
				"(Tenés un margen de error +-2. Ejemplo: si decís 5 y tardo entre 3 y 7, ganás.)");
		lblTxtinformativo.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 12));
		lblTxtinformativo.setBounds(10, 237, 474, 22);
		add(lblTxtinformativo);

		JLabel imagenFondo = new JLabel(JuegoSettings.IMG_FONDO);
		imagenFondo.setBounds(0, 0, 640, 480);
		add(imagenFondo);
	}
}

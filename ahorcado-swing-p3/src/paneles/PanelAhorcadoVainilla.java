package paneles;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import ahorcado.AhorcadoVainilla;
import ahorcado.JuegoSettings;
import ahorcado.JuegoSettings.Modalidad;
import ahorcado.JuegoSettings.Dificultad;
import ahorcado.JuegoSettings.Idioma;

import java.awt.Cursor;

@SuppressWarnings("serial")
public class PanelAhorcadoVainilla extends JPanel {
	private AhorcadoVainilla _ahorcado;
	private JTextField _inputLetra;

	public PanelAhorcadoVainilla(Idioma idioma, Dificultad dificultad, Modalidad modalidad) {
		setSize(640, 480);
		setLayout(null);

		_ahorcado = new AhorcadoVainilla(idioma, dificultad, modalidad, JuegoSettings.getPalabrasUsadas());
		JuegoSettings.agregarPalabraUsada(_ahorcado.getPalabra());
		
		_inputLetra = new JTextField();
		_inputLetra.setBackground(Color.WHITE);
		_inputLetra.addKeyListener(new KeyAdapter() {

			@Override
			public void keyTyped(KeyEvent e) {
				JuegoSettings.limitarInputUsuario(_inputLetra, e);
			}

		});
		_inputLetra.setFont(new Font("Dialog", Font.BOLD, 20));
		_inputLetra.setBounds(201, 85, 26, 31);
		add(_inputLetra);

		JLabel textoIngreseLetra = new JLabel("Ingrese una letra:");
		textoIngreseLetra.setFont(new Font("Dialog", Font.PLAIN, 20));
		textoIngreseLetra.setBounds(39, 85, 165, 23);
		add(textoIngreseLetra);

		JLabel textoIntentos = new JLabel(_ahorcado.darIntentos());
		textoIntentos.setFont(new Font("Dialog", Font.PLAIN, 20));
		textoIntentos.setBounds(453, 11, 148, 23);
		add(textoIntentos);

		JLabel palabraGuiones = new JLabel(_ahorcado.getPalabraGuiones());
		palabraGuiones.setFont(new Font("Dialog", Font.PLAIN, 20));
		palabraGuiones.setBounds(66, 170, 349, 43);
		add(palabraGuiones);

		JLabel textoGameOver = new JLabel();
		textoGameOver.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textoGameOver.setBounds(24, 313, 455, 43);
		add(textoGameOver);

		JLabel imagenAhorcado = new JLabel(_ahorcado.darImagenAhorcado());
		imagenAhorcado.setBounds(398, 79, 232, 300);
		add(imagenAhorcado);

		JButton botonLetra = new JButton("Dar letra");
		botonLetra.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonLetra.setBackground(Color.WHITE);
		botonLetra.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				char letraActual = _ahorcado.darPrimeraLetra(_inputLetra.getText());
				_inputLetra.setText(null);
				List<Integer> indices = _ahorcado.indicesDeLetraEnPalabra(letraActual);
				String nuevaPalabraGuiones = _ahorcado.actualizarGuionesSiAcerto(indices, palabraGuiones.getText(),
						letraActual);
				_ahorcado.restarIntentoSiFallo(indices);
				_ahorcado.reproducirSonidoAdecuado(indices, nuevaPalabraGuiones);

				// Actualizar herramientas del panel
				actualizarHerramientasDelPanel(textoIntentos, palabraGuiones, textoGameOver, imagenAhorcado, botonLetra,
						nuevaPalabraGuiones);
			}

		});
		botonLetra.setFont(new Font("Dialog", Font.PLAIN, 20));
		botonLetra.setBounds(62, 119, 126, 40);
		add(botonLetra);

		JButton botonReiniciar = new JButton("Volver a jugar");
		botonReiniciar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				_ahorcado = new AhorcadoVainilla(idioma, dificultad, modalidad, JuegoSettings.getPalabrasUsadas());
				JuegoSettings.agregarPalabraUsada(_ahorcado.getPalabra());
				List<Integer> indices = _ahorcado.indicesDeLetraEnPalabra(' ');
				String nuevaPalabraGuiones = _ahorcado.actualizarGuionesSiAcerto(indices, _ahorcado.getPalabraGuiones(),' ');

				actualizarHerramientasDelPanel(textoIntentos, palabraGuiones, textoGameOver, imagenAhorcado, botonLetra,
						nuevaPalabraGuiones);
			}

		});
		botonReiniciar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonReiniciar.setFont(new Font("Dialog", Font.PLAIN, 20));
		botonReiniciar.setBackground(Color.WHITE);
		botonReiniciar.setBounds(39, 367, 161, 40);
		add(botonReiniciar);

		JLabel imagenFondo = new JLabel(JuegoSettings.IMG_FONDO);
		imagenFondo.setBounds(0, 0, 640, 480);
		add(imagenFondo);
	}

	private void actualizarHerramientasDelPanel(JLabel textoIntentos, JLabel palabraGuiones, JLabel textoGameOver,
			JLabel imagenAhorcado, JButton botonLetra, String nuevaPalabraGuiones) {
		botonLetra.setEnabled(_ahorcado.actualizarBotonDarLetra(nuevaPalabraGuiones));
		textoGameOver.setText(_ahorcado.actualizarTextoGameOver(nuevaPalabraGuiones));
		palabraGuiones.setText(nuevaPalabraGuiones);
		textoIntentos.setText(_ahorcado.darIntentos());
		imagenAhorcado.setIcon(_ahorcado.darImagenAhorcado());
	}

}

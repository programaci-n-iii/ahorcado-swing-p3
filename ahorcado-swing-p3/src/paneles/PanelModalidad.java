package paneles;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import ahorcado.JuegoSettings.Modalidad;;

@SuppressWarnings("serial")
public class PanelModalidad extends JPanel {
	private JComboBox<Modalidad> _comboBox;

	public PanelModalidad() {
		setBounds(180, 100, 213, 143);
		setOpaque(false);
		setLayout(null);

		JLabel lblModalidadDeJuego = new JLabel("Modalidad de juego");
		lblModalidadDeJuego.setFont(new Font("Dialog", Font.BOLD, 15));
		lblModalidadDeJuego.setBounds(10, 11, 196, 15);
		add(lblModalidadDeJuego);

		_comboBox = new JComboBox<>();
		_comboBox.setModel(new DefaultComboBoxModel<>(Modalidad.values()));
		_comboBox.setBounds(10, 38, 179, 24);
		add(_comboBox);

	}

	public Modalidad darModalidadSeleccionada() {
		Modalidad modalidad = Modalidad.VAINILLA;
		for (Modalidad mod : Modalidad.values()) {
			if (_comboBox.getSelectedItem().equals(mod)) {
				modalidad = mod;
			}
		}
		return modalidad;
	}

}

package paneles;

import javax.swing.JPanel;

import ahorcado.JuegoSettings;
import javax.swing.JLabel;

@SuppressWarnings("serial")
public class PanelInicio extends JPanel {

	public PanelInicio() {
		setBounds(150, 20, 499, 475);
		setOpaque(false);
		setLayout(null);

		JLabel imagenInicio = new JLabel(JuegoSettings.IMG_INICIO);
		imagenInicio.setBounds(0, 0, 500, 475);
		add(imagenInicio);
	}
}

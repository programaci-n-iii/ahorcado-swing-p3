package paneles;

import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import ahorcado.JuegoSettings.Dificultad;;

@SuppressWarnings("serial")
public class PanelDificultad extends JPanel {
	private JComboBox<Dificultad> _comboBox;

	public PanelDificultad() {
		setBounds(180, 100, 165, 261);
		setOpaque(false);
		setLayout(null);

		JLabel textoDificultad = new JLabel("Dificultad");
		textoDificultad.setFont(new Font("Dialog", Font.BOLD, 15));
		textoDificultad.setBounds(6, 11, 104, 15);
		add(textoDificultad);

		_comboBox = new JComboBox<>();
		_comboBox.setModel(new DefaultComboBoxModel<>(Dificultad.values()));
		_comboBox.setBounds(6, 38, 133, 24);
		add(_comboBox);
	}

	public Dificultad darDificultadSeleccionada() {
		Dificultad dificultad = Dificultad.FACIL;
		for (Dificultad dif : Dificultad.values()) {
			if (_comboBox.getSelectedItem().equals(dif)) {
				dificultad = dif;
			}
		}
		return dificultad;
	}
}

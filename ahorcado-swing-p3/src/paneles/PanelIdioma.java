package paneles;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import javax.swing.JComboBox;
import ahorcado.JuegoSettings.Idioma;

@SuppressWarnings("serial")
public class PanelIdioma extends JPanel {
	private JComboBox<Idioma> _comboBox;

	public PanelIdioma() {
		setBounds(180, 100, 171, 161);
		setOpaque(false);
		setLayout(null);
		
		JLabel textoIdioma = new JLabel("Idioma");
		textoIdioma.setFont(new Font("Dialog", Font.BOLD, 15));
		textoIdioma.setBounds(6, 11, 123, 21);
		add(textoIdioma);
		
		_comboBox = new JComboBox<>();
		_comboBox.setModel(new DefaultComboBoxModel<>(Idioma.values()));
		_comboBox.setBounds(6, 33, 123, 24);
		add(_comboBox);
	}

	public Idioma darIdiomaSeleccionado() {
		Idioma idioma = Idioma.ESPANOL;
		for(Idioma idiom: Idioma.values()) {
			if(_comboBox.getSelectedItem().equals(idiom)) {
				idioma = idiom;
			}
		}
		return idioma;
	}
	

}

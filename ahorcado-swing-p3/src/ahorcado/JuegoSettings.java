package ahorcado;

import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import paneles.PanelAhorcadoVainilla;
import paneles.PanelDificultad;
import paneles.PanelIdioma;
import paneles.PanelInicio;
import paneles.PanelMaquinaAhorcada;
import paneles.PanelModalidad;

public class JuegoSettings {

	private final static File SONIDO_PERDIO = new File("sounds/Perder.wav"),
			SONIDO_FALLO = new File("sounds/Fallar.wav"), SONIDO_GANO = new File("sounds/Ganar.wav"),
			SONIDO_ACERTO = new File("sounds/Acertar.wav");

	private final static File PALABRAS_FACILES = new File("words/espanol/palabras-faciles.txt"),
			PALABRAS_NORMALES = new File("words/espanol/palabras-normales.txt"),
			PALABRAS_DIFICILES = new File("words/espanol/palabras-dificiles.txt"),
			PALABRAS_ALEATORIA = new File("words/espanol/palabras-aleatoria.txt");

	private final static File WORDS_FACILES = new File("words/ingles/palabras-faciles.txt"),
			WORDS_NORMALES = new File("words/ingles/palabras-normales.txt"),
			WORDS_DIFICILES = new File("words/ingles/palabras-dificiles.txt"),
			WORDS_ALEATORIA = new File("words/ingles/palabras-aleatoria.txt");

	public final static ImageIcon IMG_AHORCADO_ESTADO1 = new ImageIcon("images/estado1.png"),
			IMG_AHORCADO_ESTADO2 = new ImageIcon("images/estado2.png"),
			IMG_AHORCADO_ESTADO3 = new ImageIcon("images/estado3.png"),
			IMG_AHORCADO_ESTADO4 = new ImageIcon("images/estado4.png"),
			IMG_AHORCADO_ESTADO5 = new ImageIcon("images/estado5.png"),
			IMG_AHORCADO_ESTADO6 = new ImageIcon("images/estado6.png"),
			IMG_AHORCADO_ESTADO7 = new ImageIcon("images/estado7.png"),
			IMG_AHORCADO_ESTADO8 = new ImageIcon("images/estado8.png"), IMG_FONDO = new ImageIcon("images/fondo.png"),
			IMG_INICIO = new ImageIcon("images/textoInicio.png");

	public final static List<JButton> OPCIONES_MENU = List.of(new JButton("Jugar"), new JButton("Dificultad"),
			new JButton("Idioma"), new JButton("Modalidad"), new JButton("Inicio"), new JButton("Salir"));

	public enum Dificultad {
		FACIL, NORMAL, DIFICIL, ALEATORIA;
	}

	public enum Idioma {
		ESPANOL, ENGLISH;
	}

	public enum Modalidad {
		VAINILLA, MAQUINA_AHORCADA;
	}

	public static List<JPanel> crearPaneles() {
		List<JPanel> paneles = new ArrayList<>();
		paneles.add(new PanelDificultad());
		paneles.add(new PanelIdioma());
		paneles.add(new PanelModalidad());
		paneles.add(new PanelInicio());
		return paneles;
	}

	public static JPanel crearPanelSegunModalidad(Idioma idioma, Dificultad dificultad, Modalidad modalidad) {
		if (modalidad.equals(Modalidad.MAQUINA_AHORCADA)) {
			PanelMaquinaAhorcada maquina = new PanelMaquinaAhorcada();
			return maquina;
		} else {
			PanelAhorcadoVainilla vainilla = new PanelAhorcadoVainilla(idioma, dificultad, modalidad);
			return vainilla;
		}
	}

	public static List<String> darPalabrasFacil(Idioma idioma) { // hasta tres letras distintas
		if (idioma.equals(Idioma.ESPANOL))
			return darLineasDelArchivo(PALABRAS_FACILES);
		else
			return darLineasDelArchivo(WORDS_FACILES);
	}

	public static List<String> darPalabrasNormal(Idioma idioma) {// entre cuatro y cinco letras distintas
		if (idioma.equals(Idioma.ESPANOL))
			return darLineasDelArchivo(PALABRAS_NORMALES);
		else
			return darLineasDelArchivo(WORDS_NORMALES);
	}

	public static List<String> darPalabrasDificil(Idioma idioma) {// al menos seis letras distintas
		if (idioma.equals(Idioma.ESPANOL))
			return darLineasDelArchivo(PALABRAS_DIFICILES);
		else
			return darLineasDelArchivo(WORDS_DIFICILES);
	}

	public static List<String> darPalabrasAleatoria(Idioma idioma) {
		if (idioma.equals(Idioma.ESPANOL))
			return darLineasDelArchivo(PALABRAS_ALEATORIA);
		else
			return darLineasDelArchivo(WORDS_ALEATORIA);
	}
	
	private static List<String> _palabrasUsadas = new ArrayList<>();
	
	public static void agregarPalabraUsada(String palabra) {
		_palabrasUsadas.add(palabra);
		if(_palabrasUsadas.size() == 39)
			_palabrasUsadas = new ArrayList<>();
	}
	
	public static List<String> getPalabrasUsadas(){
		return _palabrasUsadas;
	}

	// Cada linea tiene una palabra
	private static List<String> darLineasDelArchivo(File archivoTxt) {
		List<String> lineas = new ArrayList<>();
		try {
			FileInputStream fis = new FileInputStream(archivoTxt);
			Scanner scanner = new Scanner(fis);
			while (scanner.hasNextLine()) {
				lineas.add(scanner.nextLine());
			}
			scanner.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return lineas;
	}

	public static void reproducirSonidoPerdio() {
		reproducirSonido(SONIDO_PERDIO);
	}

	public static void reproducirSonidoFallo() {
		reproducirSonido(SONIDO_FALLO);
	}

	public static void reproducirSonidoGano() {
		reproducirSonido(SONIDO_GANO);
	}

	public static void reproducirSonidoAcerto() {
		reproducirSonido(SONIDO_ACERTO);
	}

	private static void reproducirSonido(File sonido) {
		AudioInputStream audioInputStream;
		try {
			audioInputStream = AudioSystem.getAudioInputStream(sonido.getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();
		} catch (UnsupportedAudioFileException | IOException | LineUnavailableException ex) {
			ex.printStackTrace();
		}
	}

	public static void limitarInputUsuario(JTextField input, KeyEvent e) {
		if (input.getText().length() == 1) {
			e.consume();
		}
	}

}

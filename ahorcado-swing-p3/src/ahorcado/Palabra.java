package ahorcado;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import ahorcado.JuegoSettings.*;

public class Palabra {
	private List<String> _palabras;
	private String _palabraActual;
	private String _palabraGuiones;
	private List<String> _palabrasUsadas;

	public Palabra(Dificultad dificultad, Idioma idioma) {
		generarPalabras(dificultad, idioma);
		_palabraActual = palabraRandom();
	}
	
	public Palabra(Dificultad dificultad, Idioma idioma, List<String> palabrasUsadas) {
		_palabrasUsadas = palabrasUsadas;
		generarPalabras(dificultad, idioma);
		_palabraActual = palabraRandom();
	}

	private void generarPalabras(Dificultad dificultad, Idioma idioma) {
		if (dificultad.equals(Dificultad.ALEATORIA)) {
			_palabras = JuegoSettings.darPalabrasAleatoria(idioma);
		} else if (dificultad.equals(Dificultad.FACIL)) {
			_palabras = JuegoSettings.darPalabrasFacil(idioma);
		} else if (dificultad.equals(Dificultad.NORMAL)) {
			_palabras = JuegoSettings.darPalabrasNormal(idioma);
		} else {
			_palabras = JuegoSettings.darPalabrasDificil(idioma);
		}
	}

	private String palabraRandom() {
		Random indiceRandom = new Random();
		String palabra;
		int indice;
		
		indice = indiceRandom.nextInt(_palabras.size());
		palabra = _palabras.get(indice);
		
		if(_palabrasUsadas!=null && _palabrasUsadas.contains(palabra)) {
			while(_palabrasUsadas.contains(palabra)) {
				indice = indiceRandom.nextInt(_palabras.size());
				palabra = _palabras.get(indice);
			}
		}		
		return palabra;
	}

	public String crearGuiones() {
		StringBuilder palabra = new StringBuilder();
		for (int i = 0; i < _palabraActual.length(); i++) {
			palabra.append("_ ");
		}
		_palabraGuiones = palabra.toString();
		return _palabraGuiones;
	}

	public List<Integer> darIndicesDeLetraEnPalabra(char letra) {
		List<Integer> indices = new ArrayList<Integer>();
		for (int i = 0; i < _palabraActual.length(); i++) {
			if (_palabraActual.charAt(i) == letra)
				indices.add(i);
		}
		return indices;
	}

	public String getPalabraActual() {
		return _palabraActual;
	}
	
	public void actualizarPalabraActual() {
		_palabraActual = palabraRandom();
	}

}

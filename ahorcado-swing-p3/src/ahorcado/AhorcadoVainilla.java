package ahorcado;

import java.util.List;

import javax.swing.ImageIcon;

import ahorcado.JuegoSettings.Dificultad;
import ahorcado.JuegoSettings.Idioma;
import ahorcado.JuegoSettings.Modalidad;

public class AhorcadoVainilla {
	private Integer intentos;
	private String _palabraGuiones;
	private Palabra palabra;

	public AhorcadoVainilla(Idioma idioma, Dificultad dificultad, Modalidad modalidad, List<String> palabrasUsadas) {
		
		if(palabrasUsadas.isEmpty())
			palabra = new Palabra(dificultad, idioma);
		else
			palabra = new Palabra(dificultad, idioma, palabrasUsadas);
		
		_palabraGuiones = palabra.crearGuiones();

		System.out.println("La palabra random es: " + palabra.getPalabraActual());
		System.out.println("Modalidad: " + modalidad);
		System.out.println("Dificultad: " + dificultad);
		System.out.println("Idioma: " + idioma);

		intentos = 7;
	}

	public List<Integer> indicesDeLetraEnPalabra(char letra) {
		return palabra.darIndicesDeLetraEnPalabra(letra);
	}

	public String actualizarGuionesSiAcerto(List<Integer> indices, String palabraGuiones, char letra) {
		StringBuilder palabraNueva = new StringBuilder(palabraGuiones);
		if (!indices.isEmpty()) {
			for (Integer indice : indices) {
				int indiceConEspacios = indice * 2;
				palabraNueva.setCharAt(indiceConEspacios, letra);
			}
		}
		return palabraNueva.toString();
	}

	public void restarIntentoSiFallo(List<Integer> indices) {
		if (jugadorFallo(indices)) {
			intentos--;
		}
	}

	private boolean jugadorFallo(List<Integer> indices) {
		return indices.size() == 0;
	}

	public char darPrimeraLetra(String texto) {
		return texto.isEmpty() ? 0 : texto.toLowerCase().charAt(0);
	}

	public boolean actualizarBotonDarLetra(String palabraGuiones) {
		return !jugadorPerdio() && !jugadorGano(palabraGuiones);
	}

	private boolean jugadorPerdio() {
		return intentos == 0;
	}

	private boolean jugadorGano(String palabraGuiones) {
		return !palabraGuiones.contains("_");
	}

	public String actualizarTextoGameOver(String palabraGuiones) {
		if (jugadorPerdio()) {
			return crearMsjGameOverSiPierde();
		} else if (jugadorGano(palabraGuiones)) {
			return crearMsjGameOverSiGana();
		}
		return null;
	}

	private String crearMsjGameOverSiGana() {
		return "Felicidades, ganaste!!";
	}

	private String crearMsjGameOverSiPierde() {
		return "Perdiste, era: " + palabra.getPalabraActual() + ".";
	}

	public ImageIcon darImagenAhorcado() {
		switch (intentos) {
		case 0:
			return JuegoSettings.IMG_AHORCADO_ESTADO8;
		case 1:
			return JuegoSettings.IMG_AHORCADO_ESTADO7;
		case 2:
			return JuegoSettings.IMG_AHORCADO_ESTADO6;
		case 3:
			return JuegoSettings.IMG_AHORCADO_ESTADO5;
		case 4:
			return JuegoSettings.IMG_AHORCADO_ESTADO4;
		case 5:
			return JuegoSettings.IMG_AHORCADO_ESTADO3;
		case 6:
			return JuegoSettings.IMG_AHORCADO_ESTADO2;
		default:
			return JuegoSettings.IMG_AHORCADO_ESTADO1;
		}
	}

	public void reproducirSonidoAdecuado(List<Integer> indices, String palabraGuiones) {
		if (jugadorFallo(indices)) {
			if (jugadorPerdio())
				JuegoSettings.reproducirSonidoPerdio();
			else
				JuegoSettings.reproducirSonidoFallo();
		} else {
			if (jugadorGano(palabraGuiones))
				JuegoSettings.reproducirSonidoGano();
			else
				JuegoSettings.reproducirSonidoAcerto();
		}
	}

	public String darIntentos() {
		return "Intentos: " + intentos;
	}

	public String getPalabraGuiones() {
		return _palabraGuiones;
	}
	
	public String getPalabra() {
		return palabra.getPalabraActual();
	}

}

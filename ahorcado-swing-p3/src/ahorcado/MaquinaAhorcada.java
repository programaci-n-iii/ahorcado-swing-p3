package ahorcado;

import java.util.ArrayList;
import java.util.List;

public class MaquinaAhorcada {
	private String _palabraDada;
	private List<String> _palabra;
	private Integer _intentosArriesgados;
	private Integer _intentosReales;
	private int _indiceEnLetrasSegunFrecuencia;
	private String[] _letrasSegunFrecuencia;

	public MaquinaAhorcada(String palabra, String intentos) {
		_palabraDada = palabra.toLowerCase();
		denegarNumerosDePalabraDada();

		intentosArriesgados(intentos);

		_palabra = darPalabra();
		_letrasSegunFrecuencia = letrasSegunFrecuencia();
		_indiceEnLetrasSegunFrecuencia = 0;

		_intentosReales = intentosAlAdivinarPalabra();
	}

	private void denegarNumerosDePalabraDada() {
		List<String> numeros = List.of("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
		for (String numero : numeros) {
			if (_palabraDada.contains(numero)) {
				_palabraDada = _palabraDada.replace(numero, "");
			}
		}
	}

	public int intentosAlAdivinarPalabra() {
		int intentos = 0;
		String letra;
		while (!_palabra.isEmpty()) {
			letra = darLetra();
			if (_palabra.contains(letra))
				eliminarLetraEnPalabra(letra);

			intentos++;
		}
		return intentos;
	}

	public boolean jugadorGana() {
		return (Math.abs(_intentosReales - _intentosArriesgados) <= 2);
	}

	public String mensajeGameOver() {
		if (jugadorGana())
			return crearMsjGameOverSiGana();
		else
			return crearMsjGameOverSiPierde();
	}

	public void reproducirSonidoAdecuado() {
		if (jugadorGana())
			JuegoSettings.reproducirSonidoGano();
		else
			JuegoSettings.reproducirSonidoPerdio();
	}

	private void intentosArriesgados(String intentos) {
		
		String intentosVerificado = quitarLetrasEnIntentos(intentos);
		
		if (intentosVerificado.equals(""))
			_intentosArriesgados = 0;
		else
			_intentosArriesgados = Integer.parseInt(intentosVerificado);

	}

	private String quitarLetrasEnIntentos(String intentos) {
		
		String s = "";
		
		for(int i = 0; i < intentos.length(); i++) {
			if(isNumero(intentos.charAt(i)))
				s += intentos.charAt(i);
			else {
				s = "100";
				break;
			}
		}
		return s;
	}

	private boolean isNumero(char c) {
		return (c=='1' || c =='2' || c =='3' || c =='4' || c =='5' || c =='6' || c =='7' || c =='8' || c =='9');
	}

	private String crearMsjGameOverSiGana() {
		if (_intentosReales == 1)
			return "Muy bien, la máquina tardó " + _intentosReales + " intento.";
		else
			return "Muy bien, la máquina tardó " + _intentosReales + " intentos.";
	}

	private String crearMsjGameOverSiPierde() {
		if (_intentosReales == 1)
			return "Demasiado lejos, la máquina tardó " + _intentosReales + " intento.";
		else
			return "Demasiado lejos, la máquina tardó " + _intentosReales + " intentos.";
	}

	private List<String> darPalabra() {
		List<String> p = new ArrayList<>();
		for (int i = 0; i < _palabraDada.length(); i++) {
			p.add(_palabraDada.charAt(i) + "");
		}
		return p;
	}

	private String darLetra() {
		String letra = _letrasSegunFrecuencia[_indiceEnLetrasSegunFrecuencia];
		_indiceEnLetrasSegunFrecuencia++;
		return letra;
	}

	private String[] letrasSegunFrecuencia() {
		String[] letras = { "e", "a", "o", "s", "r", "n", "i", "d", "l", "c", "t", "u", "m", "p", "b", "g", "v", "y",
				"q", "h", "f", "z", "j", "ñ", "x", "k", "w" };
		return letras;
	}

	private void eliminarLetraEnPalabra(String letra) {
		List<String> nuevaPalabra = new ArrayList<>();
		for (String l : _palabra) {
			if (!l.equals(letra))
				nuevaPalabra.add(l);
		}
		_palabra = nuevaPalabra;
	}

}

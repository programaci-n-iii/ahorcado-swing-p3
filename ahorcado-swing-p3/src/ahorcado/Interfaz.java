package ahorcado;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import paneles.PanelMenu;

public class Interfaz {
	private JFrame frame;
	private PanelMenu menu;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Interfaz() {
		cambiarLookAndFeel();
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setSize(640, 480);
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		menu = new PanelMenu(frame);
		frame.getContentPane().add(menu);
	}

	private void cambiarLookAndFeel() {
		try {
			UIManager.setLookAndFeel("UpperEssential.UpperEssentialLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
	}

}
